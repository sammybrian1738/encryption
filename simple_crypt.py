# Two way encryption by simple-crypt
# Run pip3 install pycryptodome,
# then run pip3 install simple-crypt --no-dependencies

from simplecrypt import encrypt, decrypt

message = 'Hello! My name is Sammy Brian Otieno'
encrypt_passwd = 'secretpassword'

# encryption
cipher_code = encrypt(encrypt_passwd, message)

# decryption
original_message = decrypt(encrypt_passwd, cipher_code)

print(cipher_code)
print(original_message)