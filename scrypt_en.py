import pyscrypt

salt = b'jdbfdbfjbdhbhbhb nfnd fd fn df hbhbfdfdfdfd'
passwd = b'p@$Sw0rD~7'
key = pyscrypt.hash(passwd, salt, 2048, 8, 1, 32)
print("Derived key:", key.hex())