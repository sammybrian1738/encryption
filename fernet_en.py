# two way encryption
from cryptography.fernet import Fernet

message = 'This is my message'
key = Fernet.generate_key()
fernet = Fernet(key)

# encryption
encoded_msg = fernet.encrypt(message.encode())
print('Encrypted msg : ', encoded_msg)

# decryption
decoded_msg = fernet.decrypt(encoded_msg).decode()
print('Decoded msg : ', decoded_msg)