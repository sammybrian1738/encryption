# encryption by hashlib. Most
import hashlib

msg = b'This is my message'

# sha1 encryption
encoded_msg_sha1 = hashlib.sha1(msg).hexdigest()
print(encoded_msg_sha1)

# md5 encryption
encoded_msg_md5 = hashlib.md5(msg).hexdigest()
print(encoded_msg_md5)

# sha256 encryption
encoded_msg_sha256 = hashlib.sha256(msg).hexdigest()
print(encoded_msg_sha256)

# sha512 encryption
encoded_msg_sha512 = hashlib.sha512(msg).hexdigest()
print(encoded_msg_sha512)