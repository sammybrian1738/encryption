# Two way encryption by Alphanumeric encryption
# cannot call decrypt and encrypt at the same time, one must be commented out

from Crypto.Cipher import AES
from Crypto import Random

# encryption
key = b'Sixteen byte key'
iv = Random.new().read(AES.block_size)
cipher = AES.new(key, AES.MODE_CFB, iv)
msg = b'Attack at dawn'
encoded = iv + cipher.encrypt(msg)
print(encoded)

# decryption
encoded = b'\xba\x1f\x0b\xc8\xc4+\xec\xb8T\x12\xac\x9c\xe9\xee\x1c8\x02\xdag\x8d%d\xdb\x8eh\x84;\xdc\x0eU'
decoded = cipher.decrypt(encoded)[len(iv):]
print(decoded)