# One way encryption with bcrypt, more like 50-50, it can encrypt and compare
import bcrypt

password = b'mypassword'

salt = bcrypt.gensalt()

# encryption
hashed = bcrypt.hashpw(password, salt)

# comparison
new_passwd = b'new password'
if bcrypt.checkpw(new_passwd, hashed):
    print("It matches")
else:
    print("it does not match")

print(hashed)
